#ifndef ALGO_SORTING_INSERTION_SORT_H_
#define ALGO_SORTING_INSERTION_SORT_H_

#include "algo/util.h"

namespace algo
{
    template <class T>
    void insertion_sort(T* arr, size_t begin, size_t end)
    {
        // Process each element.
        for (size_t target_index = begin + 1; target_index < end; ++target_index)
        {
            // Save the target value, or "pull the card out".
            auto target_value = arr[target_index];

            // Move previous elements one position afterward
            // if the values are larger than the target value.
            // Do not use swap(), which requires an additional temporary variable.
            auto current_index = target_index;
            auto previous_index = target_index - 1;
            while (arr[previous_index] > target_value)
            {
                // Move the element afterward.
                arr[current_index] = arr[previous_index];

                // Move to the previous indices.
                --current_index;

                // Break if come to the beginning.
                if (previous_index == begin)
                {
                    break;
                }
                --previous_index;
            }

            // After all larger previous elements are moved afterward,
            // "insert the card" by assigning the target value
            // to the current index.
            arr[current_index] = target_value;
        }
    }

} // namespace algo

#endif // ALGO_SORTING_INSERTION_SORT_H_
