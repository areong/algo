#ifndef ALGO_SORTING_SELELCTION_SORT_H_
#define ALGO_SORTING_SELELCTION_SORT_H_

#include "algo/util.h"

namespace algo
{
    template <class T>
    void selection_sort(T* arr, size_t begin, size_t end)
    {
        // Regard the array consists of a sorted part (begins from `begin`) and
        // and an unsorted part (ends at `end`).
        // At each loop, move one element from the unsorted part to the sorted part.
        for (auto unsorted_begin = begin; unsorted_begin < end; ++unsorted_begin)
        {
            // Find the smallest element in the unsorted part.
            auto smallest_index = unsorted_begin;
            for (size_t i = unsorted_begin + 1; i < end; ++i)
            {
                // Check if element i is smaller.
                if (arr[i] < arr[smallest_index])
                {
                    smallest_index = i;
                }
            }

            // Swap the smallest element with the first element
            // in the unsorted part.
            swap(arr[smallest_index], arr[unsorted_begin]);

            // The smallest element is now in the sorted part, so
            // at the next loop the beginning index of the unsorted part
            // is increased by one.
        }
    }

} // namespace algo

#endif // ALGO_SORTING_SELELCTION_SORT_H_
