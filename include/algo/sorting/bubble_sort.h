#ifndef ALGO_SORTING_BUBBLE_SORT_H_
#define ALGO_SORTING_BUBBLE_SORT_H_

#include "algo/util.h"

namespace algo
{
    template <class T>
    void bubble_sort(T* arr, size_t begin, size_t end)
    {
        // Compare and swap adjacent elements pair by pair
        // until no pair is swapped.
        auto sorted = true;
        do
        {
            sorted = true;
            auto first_index = begin;
            auto second_index = begin + 1;
            while (second_index < end)
            {
                // Compare the pair.
                if (arr[first_index] > arr[second_index])
                {
                    // Swap the pair, and mark the array not sorted.
                    swap(arr[first_index], arr[second_index]);
                    sorted = false;
                }

                // Move to the next pair.
                ++first_index;
                ++second_index;
            }
        }
        while (!sorted);
    }

} // namespace algo

#endif // ALGO_SORTING_BUBBLE_SORT_H_
