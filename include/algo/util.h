#ifndef ALGO_UTIL_H_
#define ALGO_UTIL_H_

#include <iostream>

namespace algo
{
    template <class T>
    bool equal(const T* arr0, const T* arr1, size_t begin, size_t end)
    {
        for (size_t i = begin; i < end; ++i)
        {
            if (arr0[i] != arr1[i])
            {
                return false;
            }
        }
        return true;
    }

    template <class T>
    void print(const T* arr, size_t begin, size_t end)
    {
        if (begin >= end)
        {
            std::cout << "[]\n";
        }

        std::cout << "[" << arr[begin];
        for (size_t i = begin + 1; i < end; ++i)
        {
            std::cout << ", " << arr[i];
        }
        std::cout << "]\n";
    }

    template <class T>
    void swap(T& a, T& b)
    {
        T temp = a;
        a = b;
        b = temp;
    }

} // namespace algo

#endif // ALGO_UTIL_H_
