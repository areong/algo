#include <functional>
#include <iostream>
#include <gtest/gtest.h>
#include "algo/sorting/bubble_sort.h"
#include "algo/sorting/insertion_sort.h"
#include "algo/sorting/selection_sort.h"
#include "algo/util.h"

/**
 * @param[in] func a sorting function
 * @return whether the sorting result is equal to the answer.
 */
bool test_sorting(const std::function<void(int*, size_t, size_t)>& func)
{
    size_t begin = 0;
    size_t end = 6;
    int arr[] = {0, -1, 9, 4, 63, 2};
    int sorted_arr[] = {-1, 0, 2, 4, 9, 63};

    func(arr, begin, end);

    return algo::equal(arr, sorted_arr, begin, end);
}

// Use an anonymous namespace to prevent definition conflicts from Google Test.
namespace
{

    TEST(sorting, bubble_sort)
    {
        EXPECT_TRUE(test_sorting(algo::bubble_sort<int>));
    }

    TEST(sorting, insertion_sort)
    {
        EXPECT_TRUE(test_sorting(algo::insertion_sort<int>));
    }

    TEST(sorting, selection_sort)
    {
        EXPECT_TRUE(test_sorting(algo::selection_sort<int>));
    }

} // namespace
